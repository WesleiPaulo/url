const input = document.querySelector('#url');
const button = document.querySelector('#generator');
const result = document.querySelector('#result');

function friendlyUrl(text) {
  return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '') // remove acentos
    .replace(/([^\w]+|\s+)/g, '-') // substitui espaço e outros caracteres por hífen
    .replace(/\-\-+/g, '-') // substitui múltiplos hífens por um único hífen
    .replace(/(ˆ-+|-+$)/, '') // remove hífens extras do final ou do início da string
    .toLowerCase(); // deixa o texto em minúsculo
}

button.addEventListener('click', () => {
  const { value } = input;
  const url = friendlyUrl(value);
  result.innerHTML = url;
});